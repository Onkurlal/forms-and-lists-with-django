from django.urls import path
from posts.views import post_create, edit_post, posts_list, single_post

urlpatterns = [
    path("", posts_list, name="posts_list"),
    path("create/", post_create, name="post_create"),
    path("<int:id>/", single_post, name="single_post"),
    path("<int:id>/edit/", edit_post, name="edit_post"),
]
